\section{Introduction - The essence of learning}

The learning or development of any intelligent agent can be divided into 2 successive phases -
\begin{enumerate}
  \item Understanding or finding the target task. (Goal Discovery)
  \item Learning the task itself.
\end{enumerate}
These phases,  as shown in Figure~\ref{fig:LearningPhases},  need to be accomplished in a sequential manner.  Most of the work in contemporary machine learning assumes that the task to accomplish is already given and understood.  For example,  to find fraud in bank transactions,  the main objective is clearly the classification of transactions as good or bad.  The main question that remains is - How do we learn the best classifier?  One can use classical machine learning models like  ANN,  Random Forest Classifier, etc.  or a more general learning paradigm like Reinforcement Learning.  Notice, however, that RL still only deals with the second phase of learning.  This is because the task to learn is intrinsically provided to the agent in the form of a reward function.

\begin{figure}[htb]
  \centering
    \includegraphics[width=1.0\textwidth]{img/LearningPhases}
    \caption{Learning Stages.}
    \label{fig:LearningPhases}
\end{figure}

The authors of this paper,  in contrast, focus mainly on the first phase of learning. They propose an agent architecture that not only learns multiple skills but also generalizes learning to new skills. Thus their agent is an open-ended multi-skill learning agent. They use (Deep) RL to their advantage for the second phase of learning as it is a general learning paradigm. Hence the agent has to learn to formulate the goal as a reward function.  Subsequently, it has to use the reward function to learn the task.

\section{Motivation}

The inspirations of this paper come from 2 sources, namely,  \textit{Developmental Machine Learning} and \textit{Theory of Zone of proximal development}.
% The principles taken from these 2 sources act as pillars upon which the agent's learning mechanism is built.

\subsection{Developmental Machine Learning}
This is a part of machine learning that tries to mimic the ways infants try to learn new skills.  According to psychologists,  the way infants get their internal satisfaction or reward is by accomplishing novel goals.  For example, in Figure~\ref{fig:kindPlayingBlock}, after learning how to build a block tower, the child is no longer interested in repeating this task.  Even accomplishing a similar task like building a tower of books is also not very interesting or satisfying.  The novelty of the task is essential!


\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.3\textwidth}
    \includegraphics[width=1.0\textwidth]{img/kindPlayingBlock}
    \caption{A kid playing with blocks~\cite{kindPlayingBlock}.}
    \label{fig:kindPlayingBlock}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=1.0\textwidth]{img/ZPD}
    \caption{ZDP~\cite{ZPD}.}
    \label{fig:ZPD}
  \end{minipage}
\end{figure}

The authors try to mimic this in the "Curiosity Driven Exploration" of their agent.  They use construction grammar [See \ref{ConstructionGrammarSection}] as a mechanism to explore new goals.

\subsection{Theory of Zone of Proximal Development}
This theory,  developed by Russian psychologist Vygotsky,  says that there exist 3 zones of skills for any learner.  Figure~\ref{fig:ZPD} shows an illustration of these zones.  The internal zone consists of skills that the learner can learn on his own.  The middle zone,  aka \textit{Zone of Proximal Development},  consists of skills that the learner can accomplish only with the help of some guidance.   The outermost zone consists of skills that cannot be accomplished even with the help of guidance.  The key idea is this - there has to be someone to guide the child.  This is identical to the parent-child learning dynamics.  Using this principle, the authors propose the use of a social partner to guide the learning of their proposed agent.

\section{Goal Generation and Generalization}
\subsection{Previous works and their limitations}
In the previous works and this work,  each goal is given by $g \in \mathbf{R}^m$.  Using a set of known Goals $G$,  previous papers trained models such as \textit{Adversarial Neural Networks} and \textit{Variational Auto-Encoders}.  Using these models,  new goals within the known/seen distribution were created.  This is a limitation because it is not a novel goal for the agent.  E.g. The tasks - Building a stack of blocks and a stack of books are within the same goal distribution.  But they are not novel for the child.  What is novel is to jump to building a pyramid which is an out-of-distribution Goal.  The authors try to overcome this limitation by using language for generating new goals instead of the above generative models.

\subsection{Goal Generation Mechanism - Construction Grammar \cite{constructionGrammar}}
\label{ConstructionGrammarSection}

The authors make use of a concept called Construction Grammar to generate new goals. Construction Grammar can generate out of known distribution goals.  Each word, being \textbf{semantically undividable} is called an atom.  Atoms combine to form compounds which are ultimately sentences describing goals.  Figure~\ref{fig:CGFundamentals} provides an example.

\begin{figure}[htb]
  \centering
    \includegraphics[width=0.8\textwidth]{img/CGFundamentals}
    \caption{Fundamental Components of Construction Grammar.}
    \label{fig:CGFundamentals}
\end{figure}

To study the effects of Goal generalization in isolation, the authors do not use a  full blow NLP agent.  They use simple and controlled Grammar as shown in Figure~\ref{fig:ConstructionGrammarImage}.  It has 3 main actions/verbs \textbf{Go} (to a zone) \textbf{Grasp} (an object) and \textbf{Grow} (an object).  The number of objects depends on the environment's complexity.  [See \ref{EnvironmentSection} ]

\begin{figure}[htb]
  \centering
    \includegraphics[width=0.8\textwidth]{img/ConstructionGrammarImage}
    \caption{Construction Grammar used in the Paper.\cite{researchPaper}}
    \label{fig:ConstructionGrammarImage}
\end{figure}

\subsection{Goal Generation "Curiosity" algorithm}
The essence of this algorithm is that the authors try to compute equivalent atoms.  Given 2 known goals like "Move Red Table" and "Move Red Chair", the edit distance between the goals is 1.  This is because by replacing 1 word "Table" with "Chair" we can move from the first to the second goal (and vice-versa).  Now the atoms "Table" and "Chair" are considered equivalent.  One can be replaced for another in a goal to obtain a different goal.  For example "Break Red Chair" can be converted to "Break Read Table".  The new compounds/goals formed by replacing equivalent atoms are called imagined goals.  Figure~\ref{fig:CuriosityAlgorithm} shows the complete algorithm with its essence.  Using these imagined goals, the agent explores the goal space to get better goal generalization.

\begin{figure}[htb]
  \centering
    \includegraphics[width=1.0\textwidth]{img/CuriosityAlgorithm}
    \caption{Curiosity Algorithm.\cite{researchPaper}}
    \label{fig:CuriosityAlgorithm}
\end{figure}

\subsection{Goal Generalization vs State Space Generalization}
As there are 2 distinct phases of learning, as seen above,  so are there 2 distinct generalizations
\begin{itemize}
\item Goal Generalization - Corresponding to the Goal Discovery Phase.
\item State Space Generalization - Corresponding to the Task Learning Phase.
\end{itemize}

\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=1.0\textwidth]{img/GoalGeneralization}
    \caption{Goal Generalization}
    \label{fig:GoalGeneralization}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=1.0\textwidth]{img/StateSpaceGeneralization}
    \caption{State Space Generalization}
    \label{fig:StateSpaceGeneralization}
  \end{minipage}
\end{figure}

Consider a space of "Moving objects on an $x y$ plane".  Figure~\ref{fig:StateSpaceGeneralization} shows that if an agent has learned to move 1 type of object (here a chair),  then a good State-space generalization would be that the agent can apply this skill to move the chair between any 2 points in the $x y$ plane.  However, only when an agent can move a distinct object, here a table, between the same points can we claim that the agent can generalize across goals. This is illustrated in Figure~\ref{fig:GoalGeneralization}.


\section{Synthetic Environment - Playground}
\label{EnvironmentSection}
Because of the complexity reasons mentioned in \ref{ConstructionGrammarSection} the authors do not learn a full-blown Perception agent.  The environment is a 2D space in which the agent and the objects are represented as circles (internally).  3 types of objects are hardcoded in the environment - living things e.g. a plant,  a cat,  a lion, etc., non-living things e.g. a chair, a table, etc. 
and supplies i.e food and water.

The objects together with the construction grammar form a virtual farm in which the agent can Grasp and move the objects and also grow the living things.  The objects can be referenced using their predicates.   These predicates are organized in a hierarchy similar to the class hierarchy in an Object-Oriented Programming language.  The predicates aid the curiosity algorithm to make bigger jumps within the goal space.  For example, "Grow red Animal" generalized to "Grow red Plant" is a huge jump in the goal space than jumping from "Grow red Cat" to "Grow red Dog".  This is because plants can only be grown with water, unlike the animals that can grow with both water and food.  Figure~\ref{fig:EnvironmentLayout} shows the environment layout created in one instance.  Figure~\ref{fig:ObjectsAndHeirarchy} shows the hardcoded objects in the environment used for testing.

\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.35\textwidth}
    \includegraphics[width=1.0\textwidth]{img/EnvironmentLayout}
      \caption{The Playground Environment. \cite{researchPaper}}
    \label{fig:EnvironmentLayout}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.55\textwidth}
    \includegraphics[width=1.0\textwidth]{img/ObjectsAndHeirarchy}
      \caption{Objects in the Environment.\cite{researchPaper}}
    \label{fig:ObjectsAndHeirarchy}
  \end{minipage}
\end{figure}

\ifx false
\subsection{Environment Representations}
\label{EnvironmentRepresentationSection}
As the agent, in principle, is a Reinforcement Learning agent,  the following representations are defined for the environment - 
\begin{itemize}
\item object = $[d, r, g, b, ...] \in \mathbf{R}^n$ where $n \in \mathbf{I}^+$
\item agent = $[x, y, g]$ and action = $[\delta x, \delta y, \delta g]$
\item observation = $[$agent$]$ +$[$object$]_1$ +$[$object$]_2$ ...
\item state = $[$observation$]$ + $[\delta$observation$]$
\end{itemize}
Where $d \in \mathbf{R}$ is for diameter, $[x, y] \in \mathbf{R}^2$ is the position in 2D space,
$g \in \{-1, 1\}$ is wether an object is grasped or not by the agent and
$[r, g, b] \in \mathbf{R}^3$ represents Color.
\fi

\section{Language Encoder and function approximators}
The main functions of our RL agent i.e the Reward,  the Policy, and the Critic are function approximators represented by Deep Neural Networks.  Because our agent is multi-skilled, these function approximators should be conditioned on the target goal.  Because the target goal in its NL state cannot be used as a conditioner, the authors use a Long Short-Term Memory (LSTM) network to encode the goal into $\mathbf{R}^{100}$.  The LSTM network is the language encoder and is represented by the function $L_e : g \mapsto \mathbf{R}^{100} \; \forall g \in G$ where $G$ is the set of goals obtainable from the Construction Grammar.

\subsection{Reward (R),  policy($\pi$) and Critic (Q) function approximators}
Deep Neural Network is used as a function approximator for R, $\pi$, and Q functions.  In the environment, the features of objects can be appended to the state in any order. % [see \ref{EnvironmentRepresentationSection}]. 
Hence all the function approximators have to be permutation invariant.  This is accomplished by dividing the state into object-specific parts and using permutation invariant mathematical operators like $\sum$ and \textbf{OR}\cite{zaheer2018deep}.
% The reward function is learnt as a binary classifier and is defined as $R(s, g) : \mathbf{S} \times \mathbf{R}^{100} \mapsto \{0,1\} $ 


\section{Proposed Architecture}
Figure~\ref{fig:ProposedAgentArchitecture} shows the complete architecture proposed in the paper.  \textbf{IMAGINE} is an acronym which stands for \textbf{I}ntrinsic \textbf{M}otivations \textbf{A}nd \textbf{G}oal \textbf{IN}vention for \textbf{E}xploration.  Observe that Goal Generation is a part of the architecture, therefore the agent is intrinsically motivated.  The Social  Partner in the architecture does the guidance for the agent according to the \textit{Theory of Proximal Zone of Development}.  Since the reward (\textbf{R}) and policy ($\pi$) are conditioned on the goal,  the language encoder ($\mathbf{L}_{\mathbf{e}}$) is represented within the reward and the policy block.

\begin{figure}[htb]
  \centering
    \includegraphics[width=1.0\textwidth]{img/ProposedAgentArchitecture}
    \caption{Complete Architecture of the IMAGINE Agent. \cite{researchPaper}}
    \label{fig:ProposedAgentArchitecture}
\end{figure}

The basic idea of the architecture is this - make the agent explore the environment.  Store anything interesting in memory.  Thereafter, use the memory to train the function approximators.  As shown in Figure~\ref{fig:ProposedAgentArchitecture},  the authors use 2 types of memories to store the data -
\begin{itemize}
\item mem($\pi$) - To store the trajectories that the agent has experienced.
\item mem(\textbf{R}) - To store the mapping between the goal ($g$),  the state ($s$) and reward ($r$). 

$r = 1$ if $s$ is the goal state for $L_e(g)$ otherwise $r = 0$.

\end{itemize}

\section{Working of the Imagine Algorithm}
The following steps describe the working of the Imagine Algorithm
\begin{enumerate}
\item All function approximators i.e $L_e$,  R,  $\pi$ are randomly initialized.
The memories i.e mem(R),   mem($\pi$),  known Goals ($G^{known}$) and Imagined Goals($G^{Imagined}$) are initialized to empty
\item For $N_{episodes}$ goals are randomly sampled from the known and Imagined Goals.
Using $L_e$ the goal is mapped to $\mathbf{R}^{100}$
If the known and imagined goals are empty,  it samples the goal from a normal distribution.
\item Then the agent rolls out the policy conditioned on the goal i.e $\pi(s, g)$.
The trajectory of the agent is stored in mem($\pi$).
\item At the end of an episode i.e when the state of the environment is $s_T$,  the social partner "Describes" this state with a description G. 
The agent stores G as a known goal.
The agent also stores the mapping from G to $s_T$ in the mapping mem(R).
\item The agent then tries to imagine new goals from the set of known goals.
\item Batches of trajectories are then taken from mem($\pi$),  passed through the reward function to get the rewards at each state.
Using Hindsight Experience Replay,  the policy ($\pi$) is updated.
Any off-policy learning algorithm can be used for this purpose (Here the authors used DDPG).
\item Using the mapping between $s_T$ and $g$ in mem($R$),  the reward function ($R$) is trained as a classification problem and the LSTM Language encoder $L_e$ is
trained as a regression problem.
\end{enumerate}

Figure~\ref{fig:ImagineAlgorithm} shows the complete algorithm as given in the paper.

\begin{figure}[htb]
  \centering
    \includegraphics[width=1.0\textwidth]{img/ImagineAlgorithm}
    \caption{The IMAGINE algorithm.\cite{researchPaper}}
    \label{fig:ImagineAlgorithm}
\end{figure}

\section{Goal generalization}
The authors are primarily concerned with Goal Generalization in the paper.  This is because the state-space generalization achieves almost perfect generalization as reported by them ($0.95 \pm 0.05$).  2 distinct types of generalization were observed in the agent
\begin{itemize}
\item \textbf{Grow generalization to non-living things.} This can be trivially obtained by using the Construction Grammar.  If the agent has seen a goal like "Grow green animal",  the agent can replace "animal" with "table" to obtain a goal like "Grow green table".  This is very similar to a child trying to feed dolls not knowing that the dolls cannot grow.
\item \textbf{Behaviour adaptation to growing the plants.} The Social Partner does not describe or specify to the agent that plants can grow.  Nevertheless, the agent can use the Construction Grammar to create a goal like "Grow Green Plant".  However,  the reward function has been trained before using goals like "Grow Red Cat",  "Grow blue Lion" etc to give a positive reward only when the object grows.  The plants can only be grown using water and not food (unlike animals).  The agent brings both water and food to the plant to grow it because the policies before have only be trained with growing animals.  The reward only gives a positive reward when the agent brings water (because the plants only grow with water).  This whole trajectory is stored in mem($\pi$).  Subsequent training of the agent using Hindsight Experience Replay trains the policy to bring water when trying to accomplish the "Grow ... plant" goal and to bring water or food on other Goals.  This is called behavior adaptation.
\end{itemize}

\section{Experiments and Results}

\subsection{Study on Goal Imagination Mechanisms}
To test and quantify their goal generation mechanism, the authors define 2 terms called \textit{Coverage} and \textit{Precision}.  Coverage is the fraction of the goals in the test set that the agent has imagined.  Precision is the fraction of the imagined goals that are achievable.  More formally \\
Coverage $ = \frac{|G(im) \cap |G(test)|}{ |G(test)| }  $ and Precision $ = \frac{|G(im) \cap |G(achievable)|}{ |G(im)| }$.

To compare the results, the authors used Oracle (Goals only from the test set), Low coverage (Coverage is reduced by filtering out goals imagined on the test set with a particular probability), Low precision (Precision is reduced by filtering out achievable goals with a particular probability), and Random (Compounds are created by using random words) Goal generation mechanisms. Figure~\ref{fig:ExperimentsAndResults} shows you the coverage and precision values for each of the mechanisms.

\begin{figure}[htb]
  \centering
    \includegraphics[width=0.8\textwidth]{img/ExperimentsAndResults}
    \caption{Experiments and Results.\cite{researchPaper}}
    \label{fig:ExperimentsAndResults}
\end{figure}

The observation that the authors make is that the performance of an agent trained with Construction Grammar Heuristics and an oracle goal generation mechanism
is almost the same.  The training on Low coverage goal generation mechanism also gave a reasonably good performance.  However,  training on a low precision goal generation mechanism reduces the performance of the agent significantly.  Hence,  the precision of the goal imagination mechanism was found to be more important than its coverage.

\subsection{Studying the effect of changes in Social Partner}
The authors initially assume that the social partner is fully present during the training phase.  They also assume exhaustiveness i.e the SP gives perfect descriptions to the agent.  The authors reduced the exhaustiveness assumption of the Social Partner and gave "imperfect" guidance to the agent.  This reduced the performance of the agent quite significantly.

The authors then tried to strategically reduce the presence of the social partner.  The social partner was made available only in the first 10\% of the time.  This lead to almost the same performance as compared to the normal imagining agent.  This is quite similar to babies requiring extensive guidance in the initial years and later they can learn by their imagination.

Figure~\ref{fig:SocialFeedbackAnalysis}  shows the influence of social feedbacks on the performance of the agent.

\begin{figure}[htb]
  \centering
    \includegraphics[width=0.5\textwidth]{img/SocialFeedbackAnalysis}
    \caption{Influence of social Feedbacks. \cite{researchPaper}}
    \label{fig:SocialFeedbackAnalysis}
\end{figure}

\section{Review}
After seeing the above work, we now analyze the work both positively and critically.
\subsection{Positive Feedback}
Firstly, one of the novelties of the paper is that the research has brought linguistics, psychology, and computer science together to solve the problem of machine intelligence at a higher abstraction. It is a game-changer primarily because contemporary research is inclined more towards a mathematical formulation of the problem. Thus this research is orthogonal to the existing literature/research.

Secondly, as discussed in section 9.2, using the reference of child development, the authors strategically made the availability of the social partner in the first 10\% of the time. I feel it is a powerful idea because general intelligence is very complex, and the environment we deal with is stochastic. One cannot expect brute force research to give good computational models for a multi-skilled agent practically. One can take inspiration from this idea to solve other ML problems.

Lastly, the highlight of the paper was the behavior adaptation of the agent. Using this experiment, the authors show that agents can learn more general concepts. Moreover, this behavior is explainable to both the non-technical man and technical people at a conceptual level. Hence it is superior to the classical black-box unexplainable machine learning models. Nevertheless, the authors have overlooked this strength of their research. It would be nice if they could dedicate a section of their paper explaining this.

\subsection{Critical review and possible improvements}
In the paper,  one of the limitations is that the authors have completed their proof of work on a synthetic environment.  The environment itself is very simple.  The research results can get more weight if the authors used a simulator that simulates a real environment rather than a synthetic environment.  It is already done to test most of the Deep Reinforcement learning algorithms.
The algorithms are tested and trained in a simulator and then only the resulting (function approximated) policy is transferred to the hardware.  It would be nice if they employed a similar strategy to test and report their results.

The language used for the construction grammar heuristics is constrained and simple.  The most practical usage of this architecture,  however,  would be in a normal environment using a natural language.  This is because the Social Partner would be a human giving directions/guidance to the agent in a natural language.  The exploration in a goal space defined by a Natural Language grammar is not reported in the paper.  Hence the practical application of this architecture still needs investigation.

The function approximators used in the architecture are LSTM and Deep Neural Networks.  These are very powerful models and can lead to overfitting.  The authors have reported that the state space generalization is near perfect $0.95 \pm 0.05$.  Perhaps the reason for this is the usage of more powerful models.  It could be helpful if the authors reported the usage of simpler models.

Lastly,  the computation costs for training the network are very high.  As reported in the appendix,  the cost of training is 2.5 CPU years.  As the environment and the language were simple,  we can only extrapolate that the cost of training in a more practical environment and a natural language would be very expensive.  Hence it could be helpful if the authors proposed particular training algorithms that are more practical in terms of the cost of training.

\section{Future works and Conclusion}
In future works,  the authors suggest the usage of a more complex construction heuristics grammar to emulate the mechanics of natural languages.  By doing this the authors say that better agents may be trained.  Furthermore,  they also say that there needs to be more research to reduce the assumptions made for the Social Partner i.e perfect presence and perfect descriptions are given by the SP.  Currently, only when the agent stumbles upon an interesting state, the Social Partner describes it.  The authors suggest that,  instead of this,  it might be an interesting research area in which the Social Partner shows what to do and the agent mimics it.

In conclusion,  even though the work shown in this paper is just a proof of concept,  we can acknowledge that it is orthogonal to other works in the RL field.  The authors try to address the long-standing problem of goal engineering in the RL realm from a different perspective.  What is notable,  is that this paper tries to combine the ideas from diverse fields like psychology and linguistics.  Perhaps in the convergence of many fields of knowledge lies the key to building general intelligence in computing agents!

\ifx false

\section{Garbage}

% Disabling this because there is no space to put this into the summary
\section{Language - A medium for Curiosity and Guidance}
The authors use Language for both goal discovery (curiosity) as well as for guidance of the agent because
\begin{itemize}
\item In the parent-child dynamics, the parent being the social partner gives guidance to the child using language.
The authors try to mimic this.
\item Languages embody collective knowledge that has developed over 1000s of years .
They contain words that explain complex concepts using sufficiently high levels of abstractions.
\item Combinatorial composition of language makes it good for formulating and imagining new goals.
This combinatorial composition aids in the development of novel goals.
\end{itemize}
\fi

\ifx false
Figure~\ref{fig:octomap}.

\begin{figure}[htb]
  \centering
    \includegraphics[width=0.5\textwidth]{img/octomap}
    \caption{A map created with the Octomap algorithm~\cite[p.~3]{wurm2010octomap}.}
    \label{fig:octomap}
\end{figure}

\begin{equation}
	\int_0^\infty e^{-x}\mathrm{d}x = 1
\end{equation}
\fi
